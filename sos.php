
<?php include('config.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Place searches</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var map;
      var infowindow;

      function initMap() {
        var baroda = {lat: 22.3072, lng: 73.1812};

        map = new google.maps.Map(document.getElementById('map'), {
          center: baroda,
          zoom: 15
        });

        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
          location: baroda,
         // radius: 500,
         rankBy: google.maps.places.RankBy.DISTANCE,
          types: ['hospital']
        }, callback);
      }

     // console.log(places[0]+"abc");

 var xmlhttp;
	

      function callback(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < 1; i++) {
               var text=results[i];
			   var text1=JSON.stringify(text);
              var obj = JSON.parse(text1);
			  
             var rr1=obj.name;
         xmlhttp=new XMLHttpRequest();   
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState==4 &&  xmlhttp.status==200)
		{
			
			
             
	       var str=xmlhttp.responseText;
		   
		  alert(str);
			// alert(str);
			 
         
		}
		
		
	};
	xmlhttp.open("GET","fetchmap.php?rr1="+rr1,true);
	xmlhttp.send();
            //console.log(results[i]);
            createMarker(results[i]);
          }
        }
      }

	  
      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }


    </script>
  </head>
  <body>
    <div id="map"></div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8KU51I5uoFsIVR4OioDqRzYXv7jOBB8A &libraries=places&callback=initMap" async defer></script>
  </body>
</html>
