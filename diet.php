<?php
session_start();
// Check, if user is already login, then jump to secured page

include('config.php');





?>
<?php include("config.php"); ?>



<?php include('header.php');?>
<?php include('menu.php');?>

<html>
<head>
<title>Add Diet Details</title>
<style>
.form-control
{ 
width:100%;
padding:2px 2px;
height:30px;
font-size:14px;
border-radius:1px;
}
.mytable{
	width:100%;
	border:2px solid #666;
	padding:0px;
	background:#F8F8F8  ;
	box-shadow: 0px 0px 10px #AFAFAF;
	
}
.mytable td{
	
	border:2px solid #eee;
	text-align:center;
}
</style>
<?php  

$sl=mysql_query("select * from caloriedetails ");
$sll=mysql_fetch_array($sl);
$pulses=$sll['pulses'];
$fruits=$sll['fruits'];
$vegetables=$sll['vegetables'];
$dairyproducts=$sll['dairyproducts'];	
$meat=$sll['meat'];	
$total=$pulses+$fruits+$vegetables+$dairyproducts+$meat;	
	


?>

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
function myFunction()
{
	
	var array=new Array();
	
	var percent=new Array();
	var totalcalories=0;
	var pulses=document.getElementById('pulses').value;
	 array[0]=Number(<?php echo $pulses; ?>) * Number(pulses);
	
	
	var fruits=document.getElementById('fruits').value;
	array[1]=Number(<?php echo $fruits; ?>) * Number(fruits);
	
	var vegetables=document.getElementById('vegetables').value;
	array[2]=Number(<?php echo $vegetables; ?>) * Number(vegetables);
	
	var dairyproducts=document.getElementById('milk').value;
	 array[3]=Number(<?php echo $dairyproducts; ?>) * Number(dairyproducts);
	
	var meat=document.getElementById('meat').value;
	array[4]=Number(<?php echo $meat; ?>) * Number(meat);
	
	for(var i=0;i<array.length;i++)
	{
		totalcalories=totalcalories+Number(array[i]);
	}
	
	for(var h=0;h< array.length;h++)
	{
		percent[h]=(array[h]/totalcalories)*100;
	}
	var  carbohydrates1=array[1]+array[2];
	 var proteins1=array[0];
	 var cholestrol1=array[3]+array[4];
   var carbohydrates=Math.round(((array[1]+array[2])*100/totalcalories)*100)/100;
   var proteins=Math.round(((array[0]*100)/totalcalories)*100)/100;
   var cholestrol=Math.round(((array[3]+array[4])*100/totalcalories)*100)/100;
	alert("Proteins percent:"+proteins);
	alert("carbohydrates percent:"+carbohydrates);
	alert("cholestrol percent:"+cholestrol);
	 
	  google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Nutrients', 'Calories in percent'],
          ['proteins',  1],
          ['carbohyrates', 7],
          ['cholestrol',  2]
          
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
	 
	 /* var xmlhttp=new XMLHttpRequest();   
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState==4 &&  xmlhttp.status==200)
		{
			
			
             
	       var str=xmlhttp.responseText;
		   alert(str);
		  
			// alert(str);
			 
         
		}
		
		
	};
	xmlhttp.open("GET","chart.php?proteins="+proteins1+'&carbohydrates='+carbohydrates1+'&cholestrol='+cholestrol1,true);
	xmlhttp.send();*/
	//window.open('chart.php');
}	


</script>
</head>
<body>


<!-- Left side column. contains the logo and sidebar -->
<?php include('sidebar.php');?>
<!-- Left side column. contains the logo and sidebar -->

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
         <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <div style="text-align:center;"> <b style='background: #3C8DBC;padding-left:30px;padding-right:30px;
    color: white;'>Diet Analysis</b></div>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

        <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Know your diet chart</b></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post">
                  <div class="box-body">
                
				  <!---table class="mytable"--->
				   <div class="form-group">
				   
				     <label for="inputPassword3" class="col-sm-2 control-label">Number of bowls of pulses do you consume in a day? &nbsp;&nbsp;</label>
                      <div class="col-sm-10">
                      <input type="text" id="pulses" name="pulses">
                      </div>
					  </div>
					  
                      <label for="inputPassword3" class="col-sm-2 control-label">Average bowls of fruits consumed in a day? &nbsp;&nbsp;</label>
                      <div class="col-sm-10">
                        <input type="text" id="fruits" name="fruits">
                      </div>
					  </div>

                        <!---table class="mytable"--->
				   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Average bowls of vegetables consumed in a day? &nbsp;&nbsp;</label>
                      <div class="col-sm-10">
                     <input type="text" id="vegetables" name="vegetables">
					 
                      </div>
					  </div>
					  
					          <!---table class="mytable"--->
				   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Average bowls of meat consumed in a day? &nbsp;&nbsp;</label>
                      <div class="col-sm-10">
                      <input type="text" id="meat" name="meat">
                      </div>
					  </div>
					 
					<div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Average glass of milk consumed in a day? &nbsp;&nbsp;</label>
                      <div class="col-sm-10">
                      <input type="text" id="milk" name="milk">
                      </div>
					  </div>
					
					</div>
					  
					  
					 
                    <div class="box-footer">
                      <div  class="col-sm-offset-2 col-sm-10" style="text-align:center;">
                    <!--button type="submit" class="btn btn-default">Cancel</button-->
                    <button type="submit" style='margin-right:170px;'  name='bt2' onclick="myFunction();" class="btn btn-danger">Submit</button>
                  </div>
				  </div>
					  <div id="piechart" style="width: 900px; height: 500px;"></div>
<!-- /.box-body -->
				  </div>
                  <!-- /.box-footer -->
                </form>
              </div><!-- /.box -->

               


        </section><!-- /.content -->
	
					
	
	
      </div><!-- /.content-wrapper -->

      
      <footer class="main-footer">
        <?php include('footer.php');?>
      </footer>

 
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
   <script src="bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>