
<?php
session_start();
include('config.php'); ?>

<?php  
$proteins=1;
$carbohyrates=1;
$cholestrol=1;
$proteins=$_GET['proteins'];
echo $proteins;
$carbohyrates=$_GET['carbohyrates'];
$cholestrol=$_GET['cholestrol'];
?>



<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Nutrients', 'Calories in percent'],
          ['proteins',  <?php echo $proteins;?>],
          ['carbohyrates',      2],
          ['cholestrol',  2],
          
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
  </body>
</html>