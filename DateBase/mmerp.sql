-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 17, 2016 at 07:29 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mmerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `departmentname` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`sno`, `departmentname`, `code`) VALUES
(1, 'Testing', '123456'),
(3, 'Demo', '123');

-- --------------------------------------------------------

--
-- Table structure for table `sign_up`
--

CREATE TABLE IF NOT EXISTS `sign_up` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `altaddress` varchar(100) NOT NULL,
  `anniversary` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `comname` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `income` varchar(100) NOT NULL,
  `othersrvc` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`sno`),
  KEY `sno` (`sno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sign_up`
--

INSERT INTO `sign_up` (`sno`, `name`, `contact`, `pincode`, `dob`, `address`, `email`, `password`, `altaddress`, `anniversary`, `profession`, `comname`, `services`, `income`, `othersrvc`, `img`) VALUES
(6, 'admin', 'admin', '', '', '', '', 'admin', '', '', '', '', '', '', '', ''),
(7, 'guest', '0987654321', '3240091', '2016-01-01', 'kota', 'guest@gmail.com', '1234', '2', '', '3', '4', '', '', '', '0987654321.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`sno`, `department`, `username`, `designation`, `mobile`, `email`, `address`) VALUES
(1, 'Testing', 'test', 'Designation', '1231231231', 'vasimraja110@gmail.com', '  Mahaveer Nagar kota Raj'),
(3, 'Demo', 'Tst', 'post', '3213213213', 'email@gmail.com', ' kota');
